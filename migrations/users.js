
const up = function(knex) {
    return knex.schema.createTable('users', function(table){
        table.increments('id').primary();
        table.string('name', 100);
        table.string('username', 100).unique().notNullable();
        table.string('email_address').unique();
        table.string('password').notNullable();
        table.integer('login', 5);
        table.timestamps();
  
 
    })
};

const down = function(knex) {
    return knex.schema.dropTable('users');
};

module.exports = {up, down}