
const up = function(knex) {
    return knex.schema.createTable('categories', function(table){
        table.increments('id').primary();
        table.string('name', 150).notNullable(); 
    })
};

const down = function(knex) {
    return knex.schema.dropTable('categories');
};

module.exports = {up, down}