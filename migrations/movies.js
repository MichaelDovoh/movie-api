
const up = function(knex) {
    return knex.schema.createTable('movies', function(table){
        table.increments('id').primary();
        table.string('name').notNullable();
        table.integer('user_id').unsigned().nullable();
        table.date('release_year').notNullable();
        table.string('cover');
        table.boolean('deleted').defaultTo(false);
        table.datetime('deleted_at');
        table.timestamps();

        table.foreign('user_id').references('users.id');
    })
};

const down = function(knex) {
    return knex.schema.dropTable('movies');
};

module.exports = {up, down}