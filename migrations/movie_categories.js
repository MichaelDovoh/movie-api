
const up = function(knex) {
    return knex.schema.createTable('movie_categories', function(table){
        table.increments('id').primary();
        table.integer('category_id').unsigned().nullable();
        table.integer('movie_id').unsigned().nullable();
    
        table.foreign('category_id').references('categories.id');
        table.foreign('movie_id').references('movies.id');
    })
};

const down = function(knex) {
    return knex.schema.dropTable('movie_categories');
};

module.exports = {up, down}