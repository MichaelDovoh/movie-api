<h3 align="center">Movie API</h3>

---

<p align="center"> 🤖 restful api for movies.
    <br> 
</p>

## 🎈 Usage <a name = "usage"></a>

To use the `npm`, type:

```
npm run start
```

using `yarn` this project run

```bash
yarn start
```

End with an example of getting some data out of the system or using it for a little demo.

knex migration:

```
yarn knex migrate:latest
yarn knex migrate:up/down file_name.js
```

knex seeds:

```
yarn knex seed:run
yarn knex seed:run --specific=file_name.js
```

## API Reference

#### Get all movies

```http
  GET /api/movies
  Response
  {
    "status": 200,
    "message": "success",
    "data": [
        {
            "id": 11,
            "name": "Iron Man ",
            "user_id": 1,
            "release_year": "2022-03-10T00:00:00.000Z",
            "cover": "public/uploads/cover-1649516755923.jpg",
            "deleted": 0,
            "deleted_at": null,
            "created_at": "2022-04-09T15:05:56.000Z",
            "updated_at": null,
            "categories": [
                "Horror",
                "Thriller"
            ]
        }
    ]
}
```

#### Login

```http
  POST /api/auth/login
```

| Parameter  | Type     | Description  |
| :--------- | :------- | :----------- |
| `username` | `string` | **Required** |
| `password` | `string` | **Required** |

```
Response
{
    "status": 200,
    "message": "success",
    "data": {
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJtY2Nvc3RpYyIsImlhdCI6MTY0OTUxMTA3NSwiZXhwIjoxNjQ5NTk3NDc1fQ.TB8OsyTIFDH5QmNHcJt5BwcjQTLor5pGU2i2OeNQj38",
        "id": 1,
        "email_address": "mccostic@gmail.com",
        "username": "mccostic",
        "name": "mccostic"
    }
}
```

#### Register

```http
  POST /api/auth/register
```

| Parameter       | Type     | Description  |
| :-------------- | :------- | :----------- |
| `username`      | `string` | **Required** |
| `password`      | `string` | **Required** |
| `name`          | `string` | **Required** |
| `email_address` | `string` | **Required** |

#### Delete movie

```http
  DELETE /api/movies/delete/movie_id
```

| Parameter  | Type  | Description  |
| :--------- | :---- | :----------- |
| `movie_id` | `int` | **Required** |

#### Add movie

```http
  POST /api/movies/store
```

| Parameter      | Type            | Description      |
| :------------- | :-------------- | :--------------- |
| `release_year` | `date`          | **Required**     |
| `name`         | `string`        | **Required**     |
| `categories`   | `array<string>` | **Required**     |
| `cover`        | `image file`    | **Not Required** |

## ⛏️ Built Using <a name = "built_using"></a>

- [Node JS](https://praw.readthedocs.io/en/latest/) - Node Reddit API Wrapper
