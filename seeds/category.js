//Horror, Thriller, Romance, Action, Fantasy, Comedy, Adventure, Drama.
exports.seed = function (knex) {
    return knex('categories').truncate()
        .then(function () {
            return knex('categories').insert([
                {  name: 'Horror' },
                {  name: 'Thriller' },
                {  name: 'Romance' },
                {  name: 'Action' },
                {  name: 'Fantasy' },
                {  name: 'Comedy' },
                { name: 'Adventure' },
                {  name: 'Drama' },
            ]);
        });
}
