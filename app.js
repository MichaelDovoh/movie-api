const express = require('express')
const http = require('http');
const bodyParser = require('body-parser');
const cors = require('cors');
const dotenv = require('dotenv');
const port = process.env.PORT || 3001;
const path = require('path');
dotenv.config();
express.application.prefix = express.Router.prefix = function (path, configure) {
    const router = express.Router();
    console.log(path)
    this.use(`/api${path}`, router);
    configure(router);
    return router;
}
const app = express();
const server = http.createServer(app);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use("/public", express.static(path.join(__dirname, 'public')));
app.use(cors());
const api = require('./routes/api');
api(app);

server.listen(port, () => {
    console.log(`Server app listening at port : http://localhost:${port}`)
})
