'use strict';
const knex = require('../config/db_connect');
const bcrypt = require('bcryptjs');
const { auth_jwt_bearer } = require('../middleware');
const logger = require('../helper/logger');
const response = require('../helper/json_response');


const getUser = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const user = await knex('users').where('id', id).first();
        const userRes ={
            id:user.id,
            email_address:user.email_address,
            username:user.username,
            name:user.username
        }
        response.ok(res, userRes);
    }
    catch (error) {
        console.log(error);
        logger('user/getUser', error);
        res.status(500).end();
    }
}


const register = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        // auth_jwt_bearer(req, res);
        const {
            name,
            username,
            email_address,
            password
        } = req.body;

        const salt = bcrypt.genSaltSync(10)
        const passwordHash = bcrypt.hashSync(password, salt)

       try{
       
        const user  = await knex('users').where({ username })
        .orWhere({email_address}).first();
        if(user)
        return  response.error(res, "user already exist");
        await knex('users')
        .insert([{
            name,
            username,
            email_address,
            password: passwordHash,
            login: 0,
            created_at: knex.fn.now()
        }]);
       }
       catch(error){

        console.log("hvjgvhhkbj:"+error);
       }

       
        response.ok(res, "user registration successful");
    }
    catch (error) {
        console.log(error);
        logger('user/register', error);
        res.status(500).end();
    }
}


const update = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);

        const {
            id,
            name,
            username,
            email_address
        } = req.body;

        await knex('users')
            .where({ id })
            .update({
                name,
                username,
                email_address,
                updated_at: knex.fn.now()
            });
        const user = await knex('users').where({ id }).first();

        const userRes ={
            id:user.id,
            email_address:user.email_address,
            username:user.username,
            name:user.username
        }
        response.ok(res, userRes);
    }
    catch (error) {
        console.log(error);
        logger('user/update', error);
        res.status(500).end();
    }
}


const deleteUser = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);

        const { id } = req.params;
        const delData = await knex('users').where({ id }).del();
        response.ok(res, "user deleted successfully");
    }
    catch (error) {
        console.log(error);
        logger('user/deleteUser', error);
        res.status(500).end();
    }
}

const reset_password = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id, password } = req.body;
        const salt = bcrypt.genSaltSync(10)
        const passwordHash = bcrypt.hashSync(password, salt)

        await knex('users')
            .where({ id })
            .update({ password: passwordHash });
        response.ok(res, 'success reset password');
    }
    catch (error) {
        console.log(error);
        logger('user/reset_password', error);
        res.status(500).end();
    }
}

module.exports = {
    getUser,
    register,
    update,
    deleteUser,
    reset_password
}
