'use strict';
const knex = require('../config/db_connect');
const date = require('date-and-time');
const { auth_jwt_bearer } = require('../middleware');
const logger = require('../helper/logger');
const response = require('../helper/json_response');
const { datetime } = require('../helper/datetime_format');
const multer = require('multer');
const path = require('path');


const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'public/uploads/');
    },

    // By default, multer removes file extensions so let's add them back
    filename: function(req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});
const imageFilter = function(req, file, cb) {
    // Accept images only
    if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
        req.fileValidationError = 'Only image files are allowed!';
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
};

 


const movies = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end('Method not Allowed');
        const user = await auth_jwt_bearer(req, res);
        const {
            name,
            category
        } = req.query;
        console.log("name:", name)
        console.log("category:", category)
        var id = parseInt(user.id);
        let movies;
        var dataR=[];
        if(name){
            movies = await knex("movies").where('user_id', '=',id ).where('name', 'like', `%${name}%`).andWhere('deleted', 0)
        
        }
        else movies = await knex("movies").where('user_id', '=',id ).andWhere('deleted', 0);
    
        var categories;
         
        for (let index = 0; index < movies.length; index++) {
            const movie = movies[index];
            const movieId = parseInt(movie.id);
            categories = await knex.select(
            'categories.name as category',
            'categories.id as catId',
            'movie_categories.movie_id as movie_id')
            .from("categories")
                .join('movie_categories',
                 'movie_categories.category_id',
                  '=', 'categories.id')
                .where('movie_categories.movie_id',
                '=',movieId).distinct();
             const cats = categories.map(c=> c.category);
             if(category){
                const foundIndex = cats.findIndex(key => key === category);
                if(foundIndex>-1){
                    movie.categories=cats
                }
                else movie.categories=undefined;
             }
             else movie.categories=cats

             if(movie.categories){
                dataR.push(movie);
            }
             
        }
       
       
        response.ok(res, dataR);
    }
    catch (error) {
        console.log(error);
        logger('movies/show', error);
        res.status(500).end();
    }
}



const store = async function (req, res) {
    const upload = multer({ storage: storage ,fileFilter: imageFilter}).single('cover');
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const user = await auth_jwt_bearer(req, res);
        var user_id = parseInt(user.id);

       const uploadResponse = upload(req, res, async (err) =>{
        console.log("req", req.body);
        
         if (err instanceof multer.MulterError) {
            response.error(res, err);
        }
        else if (err) {
            response.error(res, err);
            //return res.send(err);
        }


        console.log("req.file.path:",req.file.path);
        const {
            name,
            cover,
            release_year,
            categories
        } = req.body;
        console.log("year",""+release_year);
        console.log("categories",categories);
        const imagePath = req.file.path;
        //const release_y = date.format(release_year, 'YYMMDD');
        const check_name = await knex('movies').where({ name });
        if (check_name.length > 0) return response.created(res, `movies name : ${name} - already exists.`);
       
        await knex('movies')
        .insert([{
            name,
            user_id,
            release_year,
            cover:imagePath,
            created_at: knex.fn.now()
        }]);
    //const movie = await knex('movies').where({ name }).andwhere({user_id}).andwhere({release_year});
    const movie = await knex('movies').where({ name }).andWhere(function() {
        this.where('user_id', '=', user_id)
      }).first();
      console.log("movie",movie);
    const movie_id = movie.id;
    for(let i=0; i< categories.length; i++){
        var category_id = categories[i];
        console.log('category_id',category_id);
        await knex('movie_categories')
        .insert([{
        category_id,
        movie_id
        }]);
    }
    response.ok(res, "movie successfully added!");
    });
    }
    catch (error) {
        console.log(error);
        logger('movies/store', error);
        res.status(500).end();
    }
}


const update = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            id,
            name,
            cover,
            release_year,
            categories
        } = req.body;
       
        const check_name = await knex('movies').where({ name });
        if (check_name.length > 0) return response.created(res, `movies name : ${name} - already exists.`);
        await knex('movies')
        .update({
            name,
            user_id,
            release_year,
            cover,
            updated_at: knex.fn.now()
        })
        .where({ id });
    //insert_channel_customer({ customer_id, email, telephone });
    const getData = await knex('movies').where({ id }).first();
    response.ok(res, getData);
    }
    catch (error) {
        console.log(error);
        logger('movie/update', error);
        res.status(500).end();
    }
}



const destroy = async  (req, res)=> {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            id
        } = req.params;
       let message;
        const check_name = await knex('movies').where({ id }).andWhere('deleted', '=', 0);
        if (check_name.length > 0){
            await knex('movies')
            .update({
                deleted:true,
                deleted_at: knex.fn.now(),
                updated_at: knex.fn.now()
            })
            .where({ id });
            message ="movie deleted successfully"
        }
        else message="movie not found or already deleted"
       
    //insert_channel_customer({ customer_id, email, telephone });
   
    response.ok(res,message);
    }
    catch (error) {
        console.log(error);
        logger('movie/deleted', error);
        res.status(500).end();
    }
}


module.exports = {
    movies,
    store,
    update,
    destroy,
}
