'use strict';
// const { auth_jwt } = require('../middleware');
const auth = require('../controller/auth_controller');
const user = require('../controller/users_controller');
const movies = require('../controller/movies_controller');

module.exports = function (app) {
    
    app.route('/').get(function (req, res) {
        res.json({ message: "Movie API running" });
        res.end();
    });
    app.prefix('/auth', function (api) {
        api.route('/login').post(auth.login);
        api.route('/logout').post(auth.logout);
    });


    app.prefix('/user',  function (api) {
        api.route('/:id').get(user.getUser);
        api.route('/register').post(user.register);
        api.route('/update').put(user.update);
        api.route('/reset_password').put(user.reset_password);
        api.route('/delete/:id').delete(user.deleteUser);
    });

       app.prefix('/movies', function (api) {
        api.route('/').get(movies.movies);
        api.route('/store').post(movies.store);
        api.route('/update').put(movies.update);
        api.route('/delete/:id').delete(movies.destroy);
    });

}
